package Facebook;

import java.util.ArrayList;
import java.util.List;

public class FacebookGraph {
    private ArrayList<User> users;

    public FacebookGraph() {
        users = new ArrayList<>();
    }

    public void addUser(User user){
        users.add(user);
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void makeFriends(String user1, String user2){
        if(!verifyRepeatedName(user1,user2) || !verifyRepeatedFriend(user1,user2)){
            User firstUser = getUser(user1);
            User secondUser = getUser(user2);

            firstUser.addFriendsList(secondUser);
            secondUser.addFriendsList(firstUser);
        }
        else{
            System.out.println("ERROR NO SE PUDO");
        }
    }

    public boolean verifyRepeatedName(String user1, String user2){
        return user1.equals(user2);
    }

    public boolean verifyRepeatedFriend(String user1, String user2){
        boolean validation = false;
        User firstUser = getUser(user1);
        List<User> friendsList = firstUser.getFriendsList();
        for (User user : friendsList){
            if(user.getName().equals(user2)) validation = true;
        }
        return validation;
    }

    private User getUser(String username){
        User userSelected = null;
        for (User user : users){
            if (user.getName().equals(username)) userSelected = user;
        }
        return userSelected;
    }

}
