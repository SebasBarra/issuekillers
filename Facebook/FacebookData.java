package Facebook;

import java.util.ArrayList;
import java.util.List;

public class FacebookData {

    private FacebookGraph facebook;
    private ArrayList<User> users;
    public FacebookData(FacebookGraph fc) {
        this.facebook = fc;
        users = facebook.getUsers();
    }

    public void showUsers(){
        for(int i = 0; i<users.size(); i++){
            System.out.println(users.get(i).getName());
        }
    }

    private void showList(User user){
        List<User> friends = user.getFriendsList();
        for(User users : friends){
            System.out.println(users.getName());
        }
    }

    public void showUsersFriends(String name){
        System.out.println("El usuario " + name + " Tiene como amigos : ");
        for(User user : users){
            if(user.getName().equals(name)){
                showList(user);
            }
        }
    }
}
