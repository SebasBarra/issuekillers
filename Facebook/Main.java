package Facebook;


public class Main {
    public static void main(String args[]){

        FacebookGraph facebook = new FacebookGraph();
        facebook.addUser(new User("Sebastian Barra"));
        facebook.addUser(new User("Gaston Gutierrez"));
        facebook.addUser(new User("Emanuel Galindo"));
        facebook.addUser(new User("Victor Cespedes"));
        facebook.addUser(new User("Cristobal Colon"));
        facebook.addUser(new User("Hernan Cortez"));

        FacebookData fc= new FacebookData(facebook);

        facebook.makeFriends("Sebastian Barra","Cristobal Colon");
        facebook.makeFriends("Sebastian Barra","Victor Cespedes");
        facebook.makeFriends("Sebastian Barra","Gaston Gutierrez");
        fc.showUsersFriends("Sebastian Barra");
        facebook.makeFriends("Cristobal Colon","Hernan Cortez");
        facebook.makeFriends("Cristobal Colon","Gaston Gutierrez");
        fc.showUsersFriends("Cristobal Colon");
    }


}
