package Facebook;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private List<User> friendsList;

    public User(String name) {
        this.name = name;
        friendsList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<User> getFriendsList() {
        return friendsList;
    }

    public void addFriendsList(User user) {
        friendsList.add(user);
    }
}
