package Facebook;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

class FacebookGraphTest {
    @Test
    public void testAddingFriendWithRepeatedName(){
        FacebookGraph facebook = new FacebookGraph();
        facebook.addUser(new User("Sebastian Barra"));

        FacebookData fc= new FacebookData(facebook);

        assertTrue(facebook.verifyRepeatedName("Sebastian Barra", "Sebastian Barra"));
    }

    @Test
    public void TestAddingRegisteredFriend(){
        FacebookGraph facebook = new FacebookGraph();
        facebook.addUser(new User("Sebastian Barra"));
        facebook.addUser(new User("Gaston Gutierrez"));
        facebook.addUser(new User("Emanuel Galindo"));
        facebook.addUser(new User("Victor Cespedes"));
        facebook.addUser(new User("Cristobal Colon"));
        facebook.addUser(new User("Hernan Cortez"));

        FacebookData fc= new FacebookData(facebook);

        facebook.makeFriends("Sebastian Barra","Cristobal Colon");
        assertTrue(facebook.verifyRepeatedFriend("Sebastian Barra","Cristobal Colon"));

    }
}