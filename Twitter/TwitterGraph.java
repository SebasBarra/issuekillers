package Twitter;


import java.util.ArrayList;
import java.util.List;

public class TwitterGraph {
    private ArrayList<User> users;

    public TwitterGraph() {
        users = new ArrayList<>();
    }

    public void addUser(User user){
        users.add(user);
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void makeFollower(String user1, String user2){
        if(!verifyRepeatedName(user1,user2) || !verifyRepeatedFollower(user1,user2)){
            User firstUser = getUser(user1);
            User secondUser = getUser(user2);

            firstUser.addFollowersList(secondUser);
        }
    }

    private boolean verifyRepeatedName(String user1, String user2){
        return user1.equals(user2);
    }

    private boolean verifyRepeatedFollower(String user1, String user2){
        boolean validation = false;
        User firstUser = getUser(user1);
        List<User> friendsList = firstUser.getFollowersList();
        for (User user : friendsList){
            if(user.getName().equals(user2)) validation = true;
        }
        return validation;
    }

    private User getUser(String username){
        User userSelected = null;
        for (User user : users){
            if (user.getName().equals(username)) userSelected = user;
        }
        return userSelected;
    }
}
