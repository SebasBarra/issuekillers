package Twitter;


import java.util.ArrayList;
import java.util.List;

public class TwitterData {

    private TwitterGraph twitter;

    ArrayList<User> users;
    public TwitterData(TwitterGraph fc) {
        this.twitter = fc;
        users = twitter.getUsers();
    }

    public void showUsers(){
        for(int i = 0; i<users.size(); i++){
            System.out.println(users.get(i).getName());
        }
    }

    private void showList(User user){
        List<User> friends = user.getFollowersList();
        for(User users : friends){
            System.out.println(users.getName());
        }
    }

    public void showUsersFriends(String name){
        System.out.println("El usuario " + name + " Sigue a : ");
        for(User user : users){
            if(user.getName().equals(name)){
                showList(user);
            }
        }
    }

}
