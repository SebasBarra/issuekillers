package Twitter;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private List<User> followerList;

    public User(String name) {
        this.name = name;
        followerList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<User> getFollowersList() {
        return followerList;
    }

    public void addFollowersList(User user) {
        followerList.add(user);
    }
}
