package Twitter;

public class Main {
    public static void main(String args[]) {


        TwitterGraph twitterGraph = new TwitterGraph();

        twitterGraph.addUser(new User("Sebastian Barra"));
        twitterGraph.addUser(new User("Gaston Gutierrez"));
        twitterGraph.addUser(new User("Emanuel Galindo"));
        twitterGraph.addUser(new User("Victor Cespedes"));
        twitterGraph.addUser(new User("Cristobal Colon"));

        TwitterData twitterData = new TwitterData(twitterGraph);

        twitterGraph.makeFollower("Sebastian Barra","Gaston Gutierrez");

        twitterGraph.makeFollower("Emanuel Galindo","Cristobal Colon");
        twitterGraph.makeFollower("Emanuel Galindo","Sebastian Barra");

        twitterData.showUsersFriends("Emanuel Galindo");


    }
}